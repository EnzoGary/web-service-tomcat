package org.example.helloworld;

import jakarta.json.Json;
import jakarta.json.JsonObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/patient")
public class Patient {
    @GET
    @Produces("application/json")
    @Path("{patientID}")
    public Response getPatient(@PathParam("patientID") Integer patientID) {
        JsonObject response = Json.createObjectBuilder()
                .add("name", "Thomas")
                .add("lastName", "Smith")
                .add("dateOfBrith", "13-03-1998")
                .add("answers", Json.createArrayBuilder()
                        .add(Json.createObjectBuilder()
                                .add("questionCovid", "Yes"))
                        .add(Json.createObjectBuilder()
                                .add("questionCardio", "Yes"))
                        .add(Json.createObjectBuilder()
                                .add("questionDiabetes", "No"))
                        .add(Json.createObjectBuilder()
                                .add("questionRespiratory", "No"))
                        .add(Json.createObjectBuilder()
                                .add("questionCancer", "Yes")))
                .build();
        return Response
                .ok(response.toString())
                .build();
    }
}
