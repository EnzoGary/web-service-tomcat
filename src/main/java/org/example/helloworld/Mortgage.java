package org.example.helloworld;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

@Path(  "/mortgage")
public class Mortgage {
    @GET
    @Produces("application/json")
    public Response computeMortgage(@QueryParam("years") Integer years,
                                    @QueryParam("interest") Double interest,
                                    @QueryParam("loanAmount") Double loanAmount,
                                    @QueryParam("annualTax") Double annualTax,
                                    @QueryParam("annualInsuranceCost") Double annualInsuranceCost,
                                    @QueryParam("openingCost") Double openingCost/*,
                                    @QueryParam("startingDate") Date startingDate*/) {
            if (years == null || interest == null || loanAmount == null || annualTax == null || annualInsuranceCost == null || openingCost == null /*|| startingDate == null*/)
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity("{\"error\": \"Missing required parameters.\"}")
                    .build();

        double monthlyInterest = interest/12/100;
        double totalPaymentsNumber = years * 12;
        double mortgagePayment = loanAmount * monthlyInterest / (1 - 1/ Math.pow(1 + monthlyInterest, totalPaymentsNumber));
        double totalMonthlyPayment = mortgagePayment + annualTax/12 + annualInsuranceCost/12;
        double totalInterestAmount = (totalMonthlyPayment * totalPaymentsNumber) - loanAmount;

        return Response
                .status(200)
                .entity("{\"totalMonthlyPayment\":"+ totalMonthlyPayment +", \"totalInterestAmount\":"+ totalInterestAmount +"}").build();
    };
}
