package org.example.helloworld;

import jakarta.json.Json;
import jakarta.json.JsonObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/questionnaire")
public class Questionnaire {
    @GET
    @Produces("application/json")
    @Path("/presentation")
    public Response getContent() {
        JsonObject response = Json.createObjectBuilder()
                .add("content", Json.createObjectBuilder()
                        .add("header", "Questionnaire")
                        .add("body", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."))
                .build();
        return Response
                .ok(response.toString())
                .build();
    }

    @GET
    @Produces("application/json")
    @Path("/questions")
    public Response getQuestions() {
        JsonObject response = Json.createObjectBuilder()
                .add("questions", Json.createArrayBuilder()
                        .add(Json.createObjectBuilder()
                                .add("questionCovid", "Have you been vaccinated against covid during the last year ?")
                                .add("options", Json.createArrayBuilder()
                                        .add("Yes")
                                        .add("No")
                                        .add("Not sure")))
                        .add(Json.createObjectBuilder()
                                .add("questionCardio", "Do you have any history of cardiovascular diseases ?")
                                .add("options", Json.createArrayBuilder()
                                        .add("Yes")
                                        .add("No")
                                        .add("Not sure")))
                        .add(Json.createObjectBuilder()
                                .add("questionDiabetes", "Have you ever been diagnosed with diabetes ?")
                                .add("options", Json.createArrayBuilder()
                                        .add("Yes")
                                        .add("No")
                                        .add("Not sure")))
                        .add(Json.createObjectBuilder()
                                .add("questionRespiratory", "Do you have any known respiratory conditions, such as asthma or COPD ?")
                                .add("options", Json.createArrayBuilder()
                                        .add("Yes")
                                        .add("No")
                                        .add("Not sure")))
                        .add(Json.createObjectBuilder()
                                .add("questionCancer", "Have you been diagnosed with any type of cancer in the past ?")
                                .add("options", Json.createArrayBuilder()
                                        .add("Yes")
                                        .add("No")
                                        .add("Not sure"))))
                .build();
        return Response
                .ok(response.toString())
                .build();
    }

    @GET
    @Produces("application/json")
    @Path("{patientID}")
    public Response getQuestion(@PathParam("patientID") Integer patientID) {
        JsonObject response = Json.createObjectBuilder()
                .build();
        return Response
                .ok(response.toString())
                .build();
    }
}
