document.addEventListener("DOMContentLoaded", () => {
    document.getElementById('questionnaire-form').addEventListener('submit', function(event) {
        event.preventDefault(); // Prevent the default form submission

        let atRisk = false;

        const formData = new FormData(this);
        for (let [name, value] of formData.entries()) {
            if (value === 'Yes') {
                atRisk = true;
                break;
            }
        }

        if (atRisk) {
            alert('You will be called back by phone for additional analysis.');
        } else {
            alert('You are ok!');
        }
    });
});
