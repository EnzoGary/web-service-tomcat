function calculateMortgage() {
    let pathname = window.location.pathname;
    let parts = pathname.split('/');
    const projectName = parts[1];

    let form = document.getElementById("mortgageForm");
    let formData = new FormData(form);
    console.log(formData);

    fetch('/'+ projectName +'/api/mortgage?' + new URLSearchParams(formData))
        .then(response => response.json())
        .then(data => {
            document.getElementById("result").innerHTML = "Total Monthly Payment: " + data.totalMonthlyPayment + "<br>" +
                "Total Interest Amount: " + data.totalInterestAmount;
        })
        .catch(error => console.error('Error:', error));
}