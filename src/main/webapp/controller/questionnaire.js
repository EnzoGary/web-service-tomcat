document.addEventListener("DOMContentLoaded", () => {
    const pathArray = window.location.pathname.split('/');
    const contextRoot = pathArray[1]; // Assuming the context root is the first part after the slash
    const baseUrl = `${window.location.origin}/${contextRoot}`;

    // Fetch presentation content
    const apiPresentationUrl = `${baseUrl}/api/questionnaire/presentation`;
    fetch(apiPresentationUrl)
        .then(response => response.json())
        .then(data => {
            document.querySelector('.header').textContent = data.content.header;
            document.querySelector('.body').textContent = data.content.body;
        })
        .catch(error => console.error('Error fetching presentation content:', error));

    // Fetch questions
    const apiQuestionsUrl = `${baseUrl}/api/questionnaire/questions`;
    fetch(apiQuestionsUrl)
        .then(response => response.json())
        .then(data => {
            const form = document.getElementById('questionnaire-form');
            data.questions.forEach((question, index) => {
                const questionKey = Object.keys(question)[0]; // Get the key of the question object
                const questionText = question[questionKey];
                const options = question.options;

                // Create a div for each question
                const questionDiv = document.createElement('div');
                questionDiv.className = 'question';

                // Add the question text
                const questionLabel = document.createElement('label');
                questionLabel.textContent = questionText;
                questionDiv.appendChild(questionLabel);

                // Add the radio buttons for each option
                options.forEach(option => {
                    const optionDiv = document.createElement('div');
                    const optionInput = document.createElement('input');
                    optionInput.type = 'radio';
                    optionInput.name = `question${index}`;
                    optionInput.value = option;

                    const optionLabel = document.createElement('label');
                    optionLabel.textContent = option;

                    optionDiv.appendChild(optionInput);
                    optionDiv.appendChild(optionLabel);
                    questionDiv.appendChild(optionDiv);
                });

                form.appendChild(questionDiv);
            });

            // Add submit button at the end of the form
            const submitButton = document.createElement('button');
            submitButton.type = 'submit';
            submitButton.textContent = 'Submit';
            form.appendChild(submitButton);
        })
        .catch(error => console.error('Error fetching questions:', error));
});
